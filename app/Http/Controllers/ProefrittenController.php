<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Proefritten;
use Illuminate\Http\Request;
use App\Http\Requests\ProefrittenRequest;
use App\Repositories\ProefrittenRepository;
use App\Repositories\ActivityLogRepository;

class ProefrittenController extends Controller
{
    protected $module = 'proefritten';

    private $request;
    private $repo;
    protected $activity;

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request, ProefrittenRepository $repo, ActivityLogRepository $activity)
    {
        $this->request  = $request;
        $this->repo     = $repo;
        $this->activity = $activity;

        $this->middleware('feature.available:proefritten');
    }

    /**
     * Used to get all Todos
     * @get ("/api/todo")
     * @return Response
     */
    public function index()
    {
        $this->authorize('view', Proefritten::class);

        return $this->ok($this->repo->paginate($this->request->all()));
    }

    /**
     * Used to store Todo
     * @post ("/api/todo")
     * @param ({
     *      @Parameter("title", type="string", required="true", description="Title of Todo"),
     *      @Parameter("date", type="date", required="true", description="Due date of Todo"),
     * })
     * @return Response
     */
    public function store(ProefrittenRequest $request)
    {
        $this->authorize('create', Proefritten::class);

        $proefritten = $this->repo->create($this->request->all());

        $this->activity->record([
            'module'   => $this->module,
            'module_id' => $proefritten->id,
            'activity' => 'added'
        ]);

        return $this->success(['message' => trans('proefritten.added')]);
    }

    /**
     * Used to get Todo detail
     * @get ("/api/todo/{id}")
     * @param ({
     *      @Parameter("id", type="integer", required="true", description="Id of Todo"),
     * })
     * @return Response
     */
    public function show($id)
    {
        $proefritten = $this->repo->findOrFail($id);

        $this->authorize('show', $proefritten);

        return $this->ok($proefritten);
    }

    /**
     * Used to update Todo status
     * @post ("/api/todo/{id}/status")
     * @param ({
     *      @Parameter("id", type="integer", required="true", description="Id of Todo"),
     * })
     * @return Response
     */
    public function toggleStatus($id)
    {
        $proefritten = $this->repo->findOrFail($id);

        $this->authorize('update', $proefritten);

        $proefritten = $this->repo->toggle($proefritten);

        $this->activity->record([
            'module'   => $this->module,
            'module_id' => $proefritten->id,
            'activity' => 'updated'
        ]);

        return $this->success(['message' => trans('proefritten.updated'),'proefritten' => $proefritten]);
    }

    /**
     * Used to update Todo
     * @patch ("/api/todo/{id}")
     * @param ({
     *      @Parameter("id", type="integer", required="true", description="Id of Todo"),
     *      @Parameter("title", type="string", required="true", description="Title of Todo"),
     *      @Parameter("date", type="date", required="true", description="Due date of Todo"),
     * })
     * @return Response
     */
    public function update($id, ProefrittenRequest $request)
    {
        $proefritten = $this->repo->findOrFail($id);

        $this->authorize('update', $proefritten);

        $proefritten = $this->repo->update($proefritten, $this->request->all());

        $this->activity->record([
            'module'   => $this->module,
            'module_id' => $proefritten->id,
            'activity' => 'updated'
        ]);

        return $this->success(['message' => trans('proefritten.updated')]);
    }

    /**
     * Used to delete Todo
     * @delete ("/api/todo/{id}")
     * @param ({
     *      @Parameter("id", type="integer", required="true", description="Id of Todo"),
     * })
     * @return Response
     */
    public function destroy($id)
    {
        $proefritten = $this->repo->findOrFail($id);

        $this->authorize('delete', $proefritten);

        $this->activity->record([
            'module'   => $this->module,
            'module_id' => $proefritten->id,
            'activity' => 'deleted'
        ]);

        $this->repo->delete($proefritten);

        return $this->success(['message' => trans('proefritten.deleted')]);
    }
}
